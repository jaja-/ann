<pre>
    __  _ __  _ __
   /  )' )  )' )  )
  /--/  /  /  /  /
 /  (_ /  (_ /  (_
</pre>

# ANN - Annotated Header

Ann is designed to allow a programmer to include the code
of their header within the source file. The code for any
parts of the header must have lines surrounding them:

```
/*\

...

\*/
```

These funtionally act like comments (so as to not interfere 
with existing source code). By passing the source into ANN, 
a header file will be created. This notation can be defined 
multiple times within the source, and will be output in the 
order in which it is found. The following syntax is supported 
for shell-like scripts:

```
#*#

...

#*#
```

This mode can be switched using the '-s' argument.

By default, the output file will be the same name as the input,
replacing the extension with '.h'. This can be altered using
the '-o' argument.

Lastly, the '-p' argument will print out the absolute path of
the output file. This can captured and used programmatically:

```
gcc -o test -include $(ann -p test.c) test.c
```

In this case, the header for "test.h" would not be necessary.


Alternatively, you may use the inline syntax for denoting a header:

```
//===

int some_function(int value);

//===

int some_function(int value) {
  ...
}
```

And for shells:

```
#===

some_functions

#===

some_scripting

```

This method has a few advantages:
 - You are not required to include the header for the code itself
 - There is no need for the `-include` flag
 - The header code can be syntax-highlighted in editors
 
With these in mind, it is recommended to use this style.


### Example

Here is a simple example of this implementation:  
Filename: annotated.c
```
#include <stdio.h>

#include "annotated.h"
/*\
void stuff();

int global;
\*/


void stuff() {
  global = 1;
  printf("Things\n");
}

int main() {
  stuff();
}

```

Once written, run: ```ann annotated.c```  
Then, run: ```gcc -o annotated annotated.c```

### Sources
[Signature source](https://patorjk.com/software/taag/#p=display&f=SL%20Script&t=ANN)

[Single instance](https://stackoverflow.com/a/17990436):
```
sed '/^[[:blank:]]*\/\*\\$/,/^[[:blank:]]*\\\*\/$/!d;//d' <input>.c
```

[Multi-instance](https://stackoverflow.com/a/17989228)
```
sed -n -e '/^[[:blank:]]*\/\*\\[[:blank:]]*$/,/^[[:blank:]]*\\\*\/[[:blank:]]*$/{ /^[[:blank:]]*\/\*\\$/d; /^[[:blank:]]*\\\*\/[[:blank:]]*$/d; p; }' "${1}" > "${2}"
sed -n -e '/^[[:blank:]]*#\*#[[:blank:]]*$/,/^[[:blank:]]*#\*#[[:blank:]]*$/{ /^[[:blank:]]*#\*#[[:blank:]]*$/d; /^[[:blank:]]*#\*#[[:blank:]]*$/d; p; }' "${1}" > "${2}"
```
